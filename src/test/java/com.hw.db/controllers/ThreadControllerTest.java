package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ThreadControllerTest {

    private ThreadController threadController;

    @BeforeEach
    void initBeforeTests() {
        threadController = new ThreadController();
    }

    @Test
    void checkIdOrSlug() {
        Thread testThread1 = new Thread("Ilsur", Timestamp.from(Instant.now()), "myForum", "msg", "slug", "title", 1604);
        Thread testThread2 = new Thread("Mansur", Timestamp.from(Instant.now()), "myForum", "msg", "slug", "title", 1604);
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(1604)).thenReturn(testThread1);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("notNumber")).thenReturn(testThread2);
            threadController.CheckIdOrSlug("1604");
            assertEquals("Ilsur", threadController.CheckIdOrSlug("1604").getAuthor());
            assertEquals("Mansur", threadController.CheckIdOrSlug("notNumber").getAuthor());
        }
    }

    @Test
    void createPost() {
        // create mock data
        String slug = "test-slug";
        List<Post> posts = new ArrayList<>();
        Post post1 = new Post();
        post1.setAuthor("John");
        post1.setMessage("Test message 1");
        posts.add(post1);
        Post post2 = new Post();
        post2.setAuthor("Jane");
        post2.setMessage("Test message 2");
        posts.add(post2);

        // create mock objects
        Thread th = new Thread();
        th.setId(1);
        th.setForum("test-forum");
        User user1 = new User();
        user1.setNickname("John");
        User user2 = new User();
        user2.setNickname("Jane");
        List<User> users = Arrays.asList(user1, user2);

        // mock dependencies
        MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class);
        MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class);

        threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(th);
        userDAOMock.when(() -> UserDAO.Info("John")).thenReturn(user1);
        userDAOMock.when(() -> UserDAO.Info("Jane")).thenReturn(user2);

        // call the method
        ResponseEntity response = threadController.createPost(slug, posts);

        // assert the response
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(posts, response.getBody());

        threadDAOMock.close();
        userDAOMock.close();
    }

    @Test
    void posts() {
        String slug = "test-slug";
        List<Post> posts = new ArrayList<>();
        Post post1 = new Post();
        post1.setAuthor("John");
        post1.setMessage("Test message 1");
        posts.add(post1);
        Post post2 = new Post();
        post2.setAuthor("Jane");
        post2.setMessage("Test message 2");
        posts.add(post2);

        // create mock objects
        Thread th = new Thread();
        th.setId(1);
        th.setForum("test-forum");
        User user1 = new User();
        user1.setNickname("John");
        User user2 = new User();
        user2.setNickname("Jane");
        List<User> users = Arrays.asList(user1, user2);

        int limit = 228;
        int since = 123;
        String sort = "mrhn";
        Boolean desc = true;

        // mock dependencies
        MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class);
        MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class);

        threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(th);
        threadDAOMock.when(() -> ThreadDAO.getPosts(th.getId(), limit, since, sort, desc)).thenReturn(posts);
        userDAOMock.when(() -> UserDAO.Info("John")).thenReturn(user1);
        userDAOMock.when(() -> UserDAO.Info("Jane")).thenReturn(user2);


        // call the method
        ResponseEntity response = threadController.Posts(slug, limit, since, sort, desc);

        // assert the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(posts, response.getBody());
        threadDAOMock.close();
        userDAOMock.close();
    }

    @Test
    void change() {
        String slug = "test-slug";

        // create mock objects
        Thread th = new Thread();
        th.setId(1);
        th.setForum("test-forum");

        // mock dependencies
        MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class);

        threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(th);

        ResponseEntity response = threadController.change(slug, new Thread());

        // assert the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        threadDAOMock.close();
    }

    @Test
    void info() {
        String slug = "test-slug";

        // create mock objects
        Thread th = new Thread();
        th.setId(1);
        th.setForum("test-forum");

        // mock dependencies
        MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class);

        threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(th);

        ResponseEntity response = threadController.info(slug);

        // assert the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(th, response.getBody());
        threadDAOMock.close();
    }

    @Test
    void createVote() {
        String slug = "test-slug";


        // create mock objects
        Thread th = new Thread();
        th.setId(1);
        th.setForum("test-forum");
        th.setVotes(50);

        String nickName = "ilsurik";
        Integer voice = 68;

        User user1 = new User();
        user1.setNickname(nickName);
        Vote vote = new Vote(nickName, voice);

        // mock dependencies
        MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class);
        MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class);

        threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(th);
        userDAOMock.when(() -> UserDAO.Info(nickName)).thenReturn(user1);

        ResponseEntity response = threadController.createVote(slug, vote);

        // assert the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(th, response.getBody());

        threadDAOMock.close();
        userDAOMock.close();
    }
}