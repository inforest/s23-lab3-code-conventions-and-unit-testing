# Lab3 -- Unit testing

## Pipeline

[![pipeline status](https://gitlab.com/inforest/s23-lab3-code-conventions-and-unit-testing/badges/master/pipeline.svg)](https://gitlab.com/inforest/s23-lab3-code-conventions-and-unit-testing/-/commits/master)

## Homework

As a homework you will need to test each of the functions in `src/main/java/com/hw/db/controllers/threadController.java` using both mocks and stubs. After that you should add your tests to the pipeline. Tests that you write should pass linter checks.
**Lab is counted as done, if pipelines are passing and tests are developed**
